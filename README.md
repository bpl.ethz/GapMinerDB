Info
=====
The GapMiner database is based on the output of the GapMiner classifier [[2]] detailed in publication [[1]], and hosted in its own repository together with all files required to for the GapMiner viewer. To host a GapMiner webserver, simply install an HTTP webserver such as Apache or nginx, and install GapMinerDB by pulling the GapMinerDB git repository at https://gitlab.com/bpl.ethz/GapMinerDB.git

License
-------
GapMiner is provided under the [Mozilla Public License 2.0](LICENSE).  

### Dependencies
The GapMiner Viewer uses [plotly.js](https://github.com/plotly/plotly.js), which is distributed under the [MIT License](https://github.com/plotly/plotly.js/blob/master/LICENSE).

Authors
-------

    Sabine Österle <sabine.oesterle@bsse.ethz.ch>
    Lukas Widmer <lukas.widmer@bsse.ethz.ch>
    Harun Mustafa <harun.mustafa@inf.ethz.ch>
    Niresh Berinpanathan <nireshb@student.ethz.ch>
    Tania Michelle Roberts <tania.roberts@bsse.ethz.ch>
    Jörg Stelling <joerg.stelling@bsse.ethz.ch>
    Sven Panke <sven.panke@bsse.ethz.ch>
    
 References
==========
[1]: http://google.com
1. S. Österle†, Lukas A. Widmer†, Harun Mustafa†, N. Berinpanathan, T. M. Roberts, J. Stelling and S. Panke (2017)  
GapMiner – Prediction of Internal Protein Tagging Sites  
† equally contributed  
[To be submitted.][1]
[2]: https://gitlab.com/bpl.ethz/GapMiner
2. [GapMiner Repository][2]