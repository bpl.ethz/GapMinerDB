//default parameters
//fraction of height taken up by probability function
var palette = [
    'rgb(228,26,28)',
    'rgb(55,126,184)',
    'rgb(77,175,74)',
    'rgb(152,78,163)',
    'rgb(255,127,0)',
    'rgb(228,26,28)',
    //'rgb(225,225,51)',
];
//default colours for sites above and below the taggability threshold
var goodcolour = 'blue';
var badcolour = 'blue';
var badbadcolour = 'blue';
//var badcolour = 'orange';
//var badbadcolour = 'red';
var sites = [];
var ordsites = [];
var goodcutoff = -1;

//these have no data, just function display parameters
prob = {
    name: 'Taggability',
	hoverinfo: 'x+y+text',
    text: 'Taggability',
    yaxis: 'y1',
    line: {color: palette[0],shape:"hvh"},
    mode: 'lines',
};
seqvar = {
    name: 'Sequence variability',
	hoverinfo: 'x+y+text',
    text: 'Sequence variability',
    yaxis: 'y2',
    line: {color: palette[1],shape:"hvh"},
    mode: 'lines',
    visible: "true",
};
lenvar = {
    name: 'Length variability',
	hoverinfo: 'x+y+text',
    text: 'Length variability',
    yaxis: 'y3',
    line: {color: palette[2],shape:"hvh"},
    mode: 'lines',
    visible: "true",
};
secstr = {
    name: 'Preservation of secondary structure',
	hoverinfo: 'x+y+text',
    text: 'Preservation of secondary structure',
    yaxis: 'y4',
    line: {color: palette[3],shape:"hvh"},
    mode: 'lines',
    visible: "true",
};
rsa = {
    name: 'Relative surface accessibility',
	hoverinfo: 'x+y+text',
    text: 'Relative surface accessibility',
    yaxis: 'y5',
    line: {color: palette[4],shape:"hvh"},
    mode: 'lines',
    visible: "true",
};
cutoff = {
    x: [0,100000],
    yaxis: 'y1',
    showlegend: false,
    hoverinfo: 'none',
    mode: 'lines',
    line: {color: 'black',dash: 'dash',},
};
evidence = {
    name: '',
    text: 'Evidence',
    yaxis: 'y6',
    line: {color:palette[5],shape:"hvh"},
    mode: 'lines',
    visible: "true",
};
var data = [prob,seqvar,lenvar,secstr,rsa,cutoff];
var xaxis_default = {side:"top",tick0: 0, title: "Residue"};

function initPlot(protdata) {
    addData(protdata);
    data[0].visible = true;
    plotDiv = document.getElementById('plots');
    layout = setDomains(true);
    //hide buttons which disable certain hover options and zoom out of bounds
    hidden = document.getElementsByClassName('afterload');
    for (i=0;i<hidden.length;i++) {
        hidden[i].style.display = "inline";
    }
    Plotly.newPlot(plotDiv, data, layout,
            {
                modeBarButtonsToRemove: ['hoverClosestCartesian','hoverCompareCartesian','autoScale2d'],
/*                modeBarButtonsToAdd: [{name: 'Toggle features', click: (plotDiv) => {Plotly.restyle(plotDiv,'visible',true,[1,2,3,4,6]);}}],*/
            }
    );
    plotDiv.on('plotly_hover', function(eventdata) {
        if(eventdata.xvals) {
            //residue number (1-based index)
            curx = eventdata.points[0].pointNumber;
            //highlight all subplots, https://github.com/plotly/plotly.js/issues/290
            axes = ['xy'];
            for (i=2;i<data.length;i++) {
                axes.push('xy'+i.toString());
            }
            Plotly.Fx.hover(plotDiv, {xval:eventdata.xvals[0] }, axes);
            //highlight current residue
            if (plotDiv.data[0].y[curx] > protdata.tagcutoff) {
                curcol = goodcolour;
            } else if (plotDiv.data[0].y[curx] > plotDiv.data[axes.length].y[0]) {
                curcol = badcolour;
            } else {
                curcol = badbadcolour;
            }
            document.getElementById('fasta').innerHTML = 
                protdata.seq.substring(0,curx-1) + 
                "<div style='color:" + curcol + "' id='curres'>" + 
                    protdata.seq.substring(curx-1,curx) + 
                "</div>" + 
                protdata.seq.substring(curx);
            //need to highlight before scrolling, otherwise scrollWidth is wrong
            seqelem = document.getElementById('seq');
            //need range to be [0%,100%]
            seqelem.scrollLeft = (curx-1)/(protdata.len-1)*(seqelem.scrollWidth-seqelem.clientWidth);
        }
    })
    .on('plotly_unhover', function(eventdata) {
        //turn off sequence highlighting
        document.getElementById('fasta').innerHTML = protdata.seq;
    })
    .on('plotly_relayout', function(eventdata) {
        //relayout if out of bounds
//        console.log('relayout');
        rx = [eventdata['xaxis.range[0]'],eventdata['xaxis.range[1]']];
        modif = false;
        if (rx[1]-rx[0] > protdata.len) {
            //reset
            rx = [0,data[0].y.length+1];
            modif = true;
        } else {
            if (rx[0] < 0) {
                //pan right
                rx = [0,rx[1]-rx[0]+1];
                modif = true;
            }
            if (rx[1] > protdata.len+1) {
                //pan left
                rx = [protdata.len+1 - (rx[1]-rx[0]),protdata.len+1];
                modif = true;
            }
        }
        if (modif) {
            lupdate = {xaxis: xaxis_default};
            lupdate.xaxis.range = rx;
            Plotly.relayout(plotDiv,lupdate);
        }
    })
    .on('plotly_redraw', function(eventdata) {
//        console.log('click');
    });
    setCutoff(false,false,false,false);

    return false;
};
function setDomains(reset) {
    nfeatures = data.length-1;
    //if plot initialised, preserve zoom, otherwise default
    plotDiv = document.getElementById('plots');
    rx = reset ? [0,data[0].y.length+1] : plotDiv.layout.xaxis.range;
    lupdate = {
        xaxis:  xaxis_default,
        shapes: sites,
        legend: {x:0,y:-0.4},
        margin: {l:10,r:10,b:0,t:50,pad:0},
    };
    lupdate.xaxis.range = rx;
    for (i=0;i<nfeatures;i++) {
        lupdate["yaxis"+(i+1).toString()] = {domain:[(nfeatures-i-1)/nfeatures,(nfeatures-i)/nfeatures], range:[0,1],showticklabels:false,zerolinewidth:2,fixedrange:true}
    }
    return lupdate;
};

function setCutoff(cutoff,maxsites,fpr,fdr) {
    uservalues = document.getElementById('cutoff');
    //if everything set to false, then reset to default values
    if (!cutoff && !maxsites && !fpr && !fdr) {
        uservalues.elements[2].value = 5;
        maxsites = true;
//        uservalues.elements[0].value = protdata.tagcutoff;
//        cutoff = true;
    }
    cutoff     = cutoff   ? uservalues.elements[0].value : 0.0;
    maxsites   = maxsites ? uservalues.elements[2].value : ordsites.length-1;
    fpr        = fpr      ? uservalues.elements[4].value : 1.0;
    fdr        = fdr      ? uservalues.elements[6].value : 1.0;
    sites = writeTaggableSites(cutoff,maxsites,fpr,fdr);
    params = sites[1] === undefined ? [1.0,undefined,0.0,1.0] : sites[1];
    sites = sites[0];
    plotDiv    = document.getElementById('plots');
    plotDiv.data[plotDiv.data.length-1].y = [params[0], params[0]];
    lupdate = {shapes: sites};
    Plotly.relayout(plotDiv,lupdate);
    Plotly.redraw(plotDiv);
    uservalues.elements[0].value = params[0];
    uservalues.elements[2].value = sites.length;
//    uservalues.elements[4].value = params[2];
    //params stores ppv=1-fdr
    //uservalues.elements[6].value = 1-params[3];
    return false;
};
function writeTaggableSites(cutoff,maxsites,fpr,fdr) {
    sites = [];
    sitesDiv = document.getElementById('sites');
    sitesDiv.innerHTML = '';
    linecol = goodcolour;
    colchanged = false;
    newtext = "<div id='goodsite' style='color:"+linecol+"'>";
    for (i=0;((i < maxsites) || (ordsites[i][0] == ordsites[i-1][0])) && (ordsites[i][0] >= cutoff) && (ordsites[i][2] <= fpr);i++) {
        if ((true) || (ordsites[i][3] <= fdr)) {
            if ((linecol == goodcolour) && (ordsites[i][0] <= protdata.tagcutoff)) {
                newtext += "</div><div id='badsite' style='color:" + badcolour + "'>";
                linecol = badcolour;
                goodcutoff = i;
                colchanged = true;
            };
            newtext += "<pre class='site'>" + ("     " + protdata.seq.substring(ordsites[i][1]-1,ordsites[i][1]) + ordsites[i][1].toString()).slice(-5) + " " + ("   " + ordsites[i][0].toFixed(3).toString()).slice(-5) + "</pre>";
            sites.push({yref: 'paper', type: 'line', y0: 0, y1: 1,line:{dash: 'dot', color: linecol,}, x0: ordsites[i][1], x1: ordsites[i][1]});
        };
    };
    newtext += "</div>";
    sitesDiv.innerHTML += newtext;
    totalheight = parseInt(window.getComputedStyle(document.getElementById('goodsite'),null).height);
    if (colchanged) {
        totalheight += parseInt(window.getComputedStyle(document.getElementById('badsite'),null).height);
    }
    document.getElementById('arrowtext').style.display = (totalheight <= parseInt(window.getComputedStyle(document.getElementById('sites'),null).height)) ? 'none' : 'inline';
    return sites.length ? [sites,ordsites[i-1]] : [[],undefined];
};

function toggleTraces(index) {
    plotDiv = document.getElementById('plots');
//    Plotly.restyle(plotDiv,'visible',!plotDiv.data[index].visible,index);
};
function toggleFold() {
    sitesbox = document.getElementById('sites');
    sitesbox.classList.toggle('unfold');
    sitesarrow.classList.toggle('unfold');
    document.getElementById('arrowtext').innerHTML = sitesbox.classList.length > 1 ? "Less" : "More";
};
function addData(protdata) {
    //insert names into html
//    document.title = "switchprot - " + protdata.shortname + " - " + protdata.name;
    document.getElementById('protname').innerHTML   = protdata.name;
    document.getElementById('organism').innerHTML   = "<i>" + protdata.organism + "</i>";
    document.getElementById('shortname').innerHTML  = "<i>" + protdata.shortname + "</i>";
    document.getElementById('uniprotid').innerHTML  = "<a href='http://www.uniprot.org/uniprot/" + protdata.uniprotid + "'>" + protdata.uniprotid + "</a>";
    document.getElementById('fasta').innerHTML      = protdata.seq;
    document.getElementById('secstruct').innerHTML  = protdata.secstruct;
    document.getElementById('annots').innerHTML = protdata.annots;
    document.getElementById('numhits').innerHTML = protdata.numhits;
    document.getElementById('classcreated').innerHTML = classifier_eval.classcreated;
    document.getElementById('dbcreated').innerHTML = protdata.dbcreated;
//    document.getElementById('meanseqvar').innerHTML = (1-protdata.meanseqvar).toFixed(3);
    document.getElementById('meanseqvar').innerHTML = (1-protdata.seqvar.reduce(function(a,b){return a+b}) / protdata.seqvar.length).toFixed(3);

    //highlight the region of the conservation score space with a lower score
//    if (protdata.meanseqvar !== undefined) {
//        document.getElementById('consdist').style.visibility = "visible";
//        rectcover.setAttribute("width",rectcoverwidth*(1-protdata.meanseqvar));
//    } else {
//        document.getElementById('consdist').style.visibility = "hidden";
//        rectcover.setAttribute("width",0);
//    };

    //genenames
    if (protdata.genenames !== undefined) {
        genenames = protdata.genenames[0];
        for (var i=1;i<protdata.genenames.length;i++) {
            genenames += ", " + protdata.genenames[i];
        }
        document.getElementById('genenames').innerHTML = genenames;
    }
    
    //update plot data
    //add initial values of 0 so residue coordinates are 1-based
    data[0].y = [0].concat(protdata.prob);
    data[1].y = [0].concat(protdata.seqvar);
    data[2].y = [0].concat(protdata.lenvar);
    data[3].y = [0].concat(protdata.secstr);
    data[4].y = [0].concat(protdata.rsa);
    data[5].y = [protdata.tagcutoff,protdata.tagcutoff];

    //map probabilities to fpr values, for thresholds

    ordsites = orderSites(data[0].y);

};
//return indices for non-increasing sorting of input array
function orderSites(probs) {
    var jlastind = 0;
    var klastind = 0;
    return probs.map(function(prob,i) {
        for (j=jlastind;j<(classifier_eval.rocthres.length) && (classifier_eval.rocthres[j] > prob);j++);
        for (k=klastind;k<(classifier_eval.prthres.length) && (classifier_eval.prthres[k] < prob);k++);
        return [prob,i,classifier_eval.fpr[j-1],classifier_eval.precision[k]];
    }).sort(function(a,b) {
        return b[0]-a[0]
    });
};
//bind 'Enter' to cutoff change button
/*document.onkeydown = function(e) {
    e = e || window.event;
    switch (e.which || e.keyCode) {
        case 13: document.getElementById('cutoff').elements[1].click();
        break;
    }
};*/

//all protein-specific data goes here
var protdata;

function loadFile(inid,procer) {
    input = document.getElementById(inid);
    file = input.files[0];
    fr = new FileReader();
    fr.onload = function(e) {
        lines = e.target.result;
        jsondata = JSON.parse(lines);
        procer(jsondata);
    };
    fr.readAsText(file)
};

function loadJSONButton() {
    document.getElementById('jsonload').classList.toggle("fileloader")
    return false;
};

function loadJSON(fromfile) {
    if (fromfile) {
        loadFile('jsoninput',function(jsondata) {
            protdata = jsondata;
            initPlot(protdata);
        });
    } else {
        //load from DB
        cursel = document.getElementById('protsel').value.split("\t");
        curselfile = "JSON/" + cursel[1]+"_"+cursel[0]+".json";
        rawFile = new XMLHttpRequest();
        rawFile.open("GET", curselfile, true);
        rawFile.onreadystatechange = function() {
            if (rawFile.readyState === 4) {
                lines = rawFile.responseText;
                jsondata = JSON.parse(lines);
                protdata = jsondata;
                initPlot(protdata);
            }
        }
        rawFile.send();
    }
};

function loadPDBJSON() {
    loadFile('pdbjsoninput',function(jsondata) {
        if (!('secondary_structure_dssp' in jsondata)) {
            alert('Invalid file');
            return false;
        };
        dssp_data = jsondata;
        for (var key in dssp_data) {
            protdata[key] = dssp_data[key];
        }
        document.getElementById('secstruct_dssp').innerHTML  = protdata.secondary_structure_dssp;
        toshow = document.getElementsByClassName('extrass');
        for (i=0;i<toshow.length;i++) {
            toshow[i].style.display = "table-cell";
        }
    });
};

function addOption(obj,text) {
    opt = document.createElement('option');
    opt.text = text;
    opt.value = text;
    obj.add(opt);
};

function loadDB() {
    //pre-load the sequence conservation distribution
//    rectcover = document.getElementById('consdist').contentDocument.getElementsByTagName('rect')[0];
//    rectcoverwidth = rectcover.getAttribute("width") * 2;
    orgsel = document.getElementById('orgsel');
    for (org in gapminer_db) {
        addOption(orgsel,org);
    };
};

function loadProts(org) {
    //this uses ALL the CPU
    protsel = document.getElementById('protsel');
    for (prot in gapminer_db[org]) {
        addOption(protsel,gapminer_db[org][prot][1] + "\t" + gapminer_db[org][prot][0]);
    };
};
