\documentclass[a4paper,12pt,openany]{memoir}
\setlength{\beforechapskip}{0pt}
% \usepackage{llncsdoc}
% \RequirePackage{cmap}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}

\usepackage[in,empty]{fullpage}
\usepackage{textcomp}
\usepackage[ddmmyyyy]{datetime}
\usepackage{amsmath,amssymb,amsfonts,mathdots,amstext}
\usepackage{moreverb}
\usepackage{graphicx,graphics}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{tikz}
\usepackage{float}
\usepackage{listings}
\usepackage{pdfpages}
% \usepackage{fancyhdr}
% \usepackage{lastpage}
% \usepackage{enumitem}
% \usepackage{setspace}
% \usepackage{datenumber}
% \usepackage{fontspec}
% \setmonofont{FreeMono}
\usepackage{mathpazo}
\PassOptionsToPackage{hyphens}{url}
\usepackage{hyperref}
\hypersetup{
  colorlinks   = true, %Colours links instead of ugly boxes
  urlcolor     = blue, %Colour for external hyperlinks
  linkcolor    = blue, %Colour of internal links
  citecolor    = red   %Colour of citations
}
%\usepackage[super,numbers]{natbib}
\usepackage[sorting=none,backend=biber,maxcitenames=20,maxnames=20,url=false,isbn=false,eprint=false,style=authoryear,firstinits=true,uniquename=init,bibencoding=utf8]{biblatex}
\bibliography{refs}
\setlength{\footskip}{11pt}
\renewcommand\bibname{References}
\renewcommand\chaptername{}
% \renewcommand\thechapter{}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}
% \relpenalty=99999
% \binoppenalty=99999

%%%% Macros %%%%
\newcommand{\ds}{\displaystyle}
\newcommand{\R}{\mathbb{R}}
% \newcommand{\C}{\mathbb{C}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\bO}{\mathcal{O}}
\newcommand{\dd}{\mathrm{d}}
\newcommand{\cok}{\mathrm{cok\hspace{0.2em}}}
\newcommand{\ddx}[2]{\frac{\mathrm{d} #1}{\mathrm{d} #2}}
\newcommand{\ppx}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\subpg}[1]{\begin{center}\begin{minipage}{0.8\textwidth}#1\end{minipage}\end{center}}

%sets figures to be in the same section, but floating at the top or bottom of a page
%http://tex.stackexchange.com/questions/140568/how-to-set-default-positioning-of-figure-table-document-wide
\makeatletter
  \providecommand*\setfloatlocations[2]{\@namedef{fps@#1}{#2}}
\makeatother
\setfloatlocations{figure}{!htb}
\setfloatlocations{table}{!htb}


\title{GapMiner User Manual}
\author{Harun Mustafa, Sabine Oesterle, Lukas Widmer}


\begin{document}
\maketitle
\section*{Citation}
\noindent\fullcite{GapMiner2017}\\
\vspace{1cm}
\tableofcontents
\sloppy
\OnehalfSpacing
\raggedbottom

\chapter{Introduction}
Protein tagging is an essential molecular biology procedure widely applied in academia and the industry for conferring new functionality to proteins of interest. For a site in a protein to be \emph{taggable}, a tag inserted at that site must be functional and accessible to binding partners, while preserving protein activity. A site which accepts modifications without affecting protein structure is known as a \emph{permissive site}.

GapMiner is a pipeline for predicting tagging sites in proteins from sequence information. For each site in a query protein, four different \emph{features} are calculated. These are used as inputs for a trained \emph{balanced random forest} classifier, which produces a probability of taggability. The assumptions underlying each respective feature are briefly, as follows:
\begin{enumerate}
 \item \emph{Sequence variability}: If it is variable in sequence identity, it is interdomain, and thus permissive.
 \item \emph{Length variability}: If indels occur there in the phylogeny, it is permissive.
 \item \emph{Relative surface accessibility}: If it is highly surface accessible, then a tag at that site is accessible as well.
 \item \emph{Structural flexibility}: If it is far from a secondary structure element (i.e. in an unstructured loop), then it is unlikely to affect protein structure (and hence is permissive) and the tag is structurally free for interaction.
\end{enumerate}
For an in-depth description of the features used for prediction, please consult the manuscript, and the supplementary methods.

\chapter{Software usage}
To avoid unnecessary computation and maximise prediction accuracy, a database of per-residue features and taggability probabilities for the \textit{E.~coli} proteome is provided. The GapMiner Viewer interface allows the user to both browse the database of results and to load results computed using the pipeline.

\section{Using taggability score database}
Using taggability scores from the provided database is recommended if the pipeline's default parameters are appropriate (see Section \ref{sec:clp}). Simply select and load the desired organism and protein from the menu system. 

\section{GapMiner Viewer}
\subsection{Loading a file}
To load a file from the database, select the organism from the drop-down menu, then select the protein (listed by gene name) from the second drop-down menu and click ``Load''. Alternatively, given a GapMiner result file in \texttt{JSON} format, load the file into the dialog below the main selector and click ``Load''. The plots should then be generated in a few seconds.

After loading, information about the protein of interest will be displayed near the top (see next page). The distribution of protein conservation measures is shown on the right to indicate the protein's conservation relative to other proteins in the database.

\subsection{Tunable parameters}
Two parameters are available for selecting which sites should be displayed as ``taggable''. When a parameter value is set, both parameter values will be adjusted to the next greatest value in the data. The parameters are as follows:
\begin{itemize}
 \item Taggability cutoff: only display sites with taggability scores above this value. Sites with taggability scores above this value, but below the default cutoff are defined as \emph{semi-taggable}.
 \item Maximum \# sites: only display this many sites
\end{itemize}
Values can be reset to their defaults by clicking on ``Default parameters''. 
\subsection{Navigating the plots}
When hovering over elements in the plots, tooltips indicate the values of each feature at that site, while the corresponding residue is coloured in the display above the plots. By default, the top five residues are coloured ``taggable'' and reported. The plots can be zoomed into by clicking and dragging to select the desired region.

If the whole screen is resized, click on ``Redraw plots'' to adjust the plots to the new screen size.

The results can be exported in a variety of formats. The plots can be exported in PNG and PDF formats, while the raw data may be exported in CSV (comma-separated values) format.

\includepdf{Figures/adk_output.pdf}

\section{Running the software locally}
If analysis of a protein not in the database or a change of parameters is required, it will be necessary to install and run the software locally. Even when run locally, the software still has the option to use homology search and structure prediction results from remote servers to reduce computational load. However, given sufficient computing power, it is recommended to run all operations and processes locally when batch analysing several proteins.

Please note that the taggability classifier is trained on features computed with the default parameters, so drastic changes may result in lower prediction accuracy. In particular, the sequence variability and length variability features are highly dependent on the set of homologs chosen to construct the protein family profile.

\subsection{Software prerequisites}
\subsubsection{Python 3}
The Anaconda 3 distribution of Python 3 comes with many of GapMiner's prerequisite packages pre-installed. After installation, however, upgrading to version $\geq0.18$ of scikit-learn and the installation of the BioPython package is required. These can be done by running the following command
\lstset{language=bash,basicstyle=\ttfamily}
\begin{lstlisting}
 conda install sklearn biopython
\end{lstlisting}
in a terminal, PowerShell, or Command Prompt.

If you are running your own distribution of Python 3, the following packages are required
\begin{itemize}
 \item BioPython $\geq1.68$ \footnote{do not use version $1.67$, see GitHub commit \texttt{f316f00}}
 \item numpy, scipy
 \item requests
 \item Beautiful Soup 4
 \item lxml
 \item scikit-learn $\geq0.18$
\end{itemize}

\subsubsection{External applications}
The following external applications are used by the pipeline
\begin{itemize}
 \item hhblits (for homology search)
 \item NetSurfP (for structure prediction, predictions can also be computed remotely)
 \item ncbi-blast+ 2.2.28+ (if NetSurfP is run locally and/or if DELTA-BLAST is used for homology searches)
 \item Clustal Omega (optional if using hhblits, required if using DELTA-BLAST)
 \item DSSP (optionally required for crystal structure-based feature calculation)
\end{itemize}
Please refer to the their respective manuals for installation instructions.\footnote{To run NetSurfP 1.0 locally, you must edit the hidden file \texttt{\$NETSURFPDIR/etc/.ncbirc} to point to the correct \texttt{BLASTDATA} directory.}

\subsection{Installation/update}
After downloading the software, extract the archive (either in \texttt{zip} or \texttt{tar.gz} format). Run the install script
\begin{lstlisting}
 gapminer-install.{bat,sh}
\end{lstlisting}
script to set up the software (use the \texttt{.bat} file for Windows and \texttt{.sh} for Mac OS and GNU/Linux platforms). The latest source may also be checked out from the git repository \url{https://gitlab.com/bpl.ethz/GapMiner.git}. This process includes the generation of a local database of the UniProtKB/Swiss-Prot proteomes for the organisms listed previously.

The configuration file \texttt{config.py} will be present in the installation folder. Please edit the following values
\begin{itemize}
 \item Set \texttt{email} to your email address (only required if running NetSurfP or DELTA-BLAST remotely)
 \item Set \texttt{netsurfp} to the path of the NetSurfP executable, or to \texttt{remote}
 \item Set \texttt{search\_db\_path} to the prefix of the HMM database to use for hhblits searches
\end{itemize}
See Section \ref{sec:clp} for an explanation of all parameters.

As new proteins are added and updated, periodic updating of the local proteome database may be required. This can be done post-installation by running 
\begin{lstlisting}
 gapminer --setup PATHS_TO_UNIPROT_XML_FILES
\end{lstlisting}
This command is included in the install script, so running it is not required during initial setup.

\subsection{Usage}
After setting up the configuration file, the following command is sufficient to run the pipeline
\begin{lstlisting}
 ./gapminer -c PATH_TO_CONFIG -P OUTPUT_PATH UNIPROTKB_ID
\end{lstlisting}
Please note that parameters set in the command line take precedence over those in the configuration file. The configuration file is not updated when setting parameters in this way.

A folder will be created in \texttt{OUTPUT\_PATH} to store all intermediate files. The final output file for viewing is stored as a \texttt{JSON} file.

\subsection{GapMiner Database / Viewer}
The GapMiner database is based on the output of the GapMiner classifier, and hosted in its own repository together with all files required to for the GapMiner viewer. To host a GapMiner webserver, simply install an HTTP webserver such as Apache or nginx, and install GapMinerDB by pulling the GapMinerDB git repository at \url{https://gitlab.com/bpl.ethz/GapMinerDB.git}.

\subsection{Troubleshooting}
Please contact Lukas Widmer (\url{lukas.widmer@bsse.ethz.ch}, GitLab user \texttt{Widmer}) or Harun Mustafa (\url{harun.mustafa@inf.ethz.ch}, GitLab user \texttt{hmusta}) if any issues arise during installation or execution of the software.

\chapter{Command line arguments and GapMiner module API}
\section{\texttt{gapminer}}
\label{sec:clp}
\begin{verbatim}
Usage: gapminer [options] [UniProtKB ID]

Predict sites for tag insertion

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  --setup               Initial setup.
  -c CONFIG, --config=CONFIG
                        Location of configuration file storing parameters.
                        Passed arguments will override these
  -p PDBID, --pdbid=PDBID
                        PDB ID and chains. Format:
                        PDBID[:CHAIN1[,CHAIN2[,...]]]. Otherwise, the
                        previously used ID will be used. If none was ever
                        provided, secondary structure will be predicted
                        computationally.
  -P PATH, --path=PATH  Path. Data will be stored in [Path]/[UniProtId]
                        (Default ".")
  --msa_input=MSA_INPUT
                        If search_method == "manual", specifies the path to an
                        MSA to use as input instead of searching and
                        filtering.
  -n THREADS, --threads=THREADS
                        The number of threads for Clustal Omega to use.
                        (Default 1)
  --debug               Print debugging information to screen.

  Recalculate intermediate steps:
    --force_blast       Force a recomputation of the BLAST results
    --force_msa         Force a recomputation of the MSA
    --force_annot       Force a refetch of the query sequence and annotation
    --force_filter      Force a refiltering of the homology search results
    --onlyoutput        Write an output file from an already processed run

  Paths and flags for external applications:
    --email=EMAIL       Email address for remote (online) NetSurfP and DELTA-
                        BLAST queries
    --search_method=SEARCH_METHOD
                        Homology search method. Can be either "hhblits",
                        "deltablast", or "manual" if a set of homologs is
                        manually provided (Default: "hhblits")
    --search_db=SEARCH_DB
                        Space-separated list of protein databases to use for
                        DELTA-BLAST searches. (Default "swissprot").
    --msa_method=MSA_METHOD
                        Method used to calculate MSA of similar sequences. If
                        hhblits is used as the search method, can be either
                        "hhblits" or "msa" to use Clustal Omega. This option
                        is ignored if search method is set to "deltablast".
    --netsurfp=NETSURFP
                        Path to NetSurfP. If set to "remote", uses the remote
                        server. (Default "remote")
    --protein_db=PROTEIN_DB
                        Prefix of the BLAST DB to use for NetSurfP if running
                        locally.
    --search_db_path=SEARCH_DB_PATH
                        hhblits: prefix of the protein database. DELTA-BLAST:
                        directory of protein and conserved domain databases.
    --localblast        Run BLAST searches from a local DB instead of using
                        the NCBI server.
    --localquery        Use a local copy of UniProtKB for fetching queries.
                        Set this option if you are running gapminer for
                        multiple queries to prevent being blocked.

  Profile construction parameters:
    --blastnum=BLASTNUM
                        The number of results blast returns (Default 500)
    --row_weight_method=ROW_WEIGHT_METHOD
                        Sequence weighting method to use (Default "helikoff"
    --minidentity=MINIDENTITY
                        The lower cutoff for sequence identity to the query.
                        (Default 0.4)
    --maxidentity=MAXIDENTITY
                        The upper cutoff for sequence identity to the query.
                        (Default 1)
    --substitution_matrix=SUBSTITUTION_MATRIX
                        Substitution matrix to use for protein-protein
                        alignments and feature functions (Default "blosum62").
                        See the documentation of the BioPython
                        Bio.SubsMat.MatrixInfo module for a list of valid
                        matrices.
    --fullalign         Fetch the full sequences for all hits when aligning
                        instead of using just the HSP
\end{verbatim}
\end{document}